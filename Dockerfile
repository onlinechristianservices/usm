FROM microsoft/dotnet:2.1-aspnetcore-runtime
EXPOSE 80
WORKDIR /app
COPY ./publish /app
ENTRYPOINT ["dotnet", "unsealingdanielsmysteries.live.dll"]
