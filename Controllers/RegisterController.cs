using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace usm
{
    [Route("api/r")]
    public class RegisterController : Controller
    {
        private readonly Repository _repo;
        private readonly MailQueue _mailQueue;
        private readonly SmsQueue _smsQueue;
        private readonly string _smsReminderText =
@"Just a reminder of the upcoming Unsealing Daniel's Mysteries 
@ [Address]
Begins [Date and Time]
We look forward to meeting you there.";

        public RegisterController(Repository repo, MailQueue mailQueue, SmsQueue smsQueue)
        {
            _repo = repo;
            _mailQueue = mailQueue;
            _smsQueue = smsQueue;
        }

        [HttpPost]
        public async Task<IActionResult> PostRegister([FromBody]Contact contact)
        {
            contact.PartitionKey = contact.Zip;
            contact.RowKey = Guid.NewGuid().ToString();
            await _repo.Save(contact);
            await _mailQueue.Send(contact);
            return Ok();
        }

        [HttpPost("sendbulk")]
        public async Task<IActionResult> SendAllRegistrations()
        {
            if (!HttpContext.Request.Headers.ContainsKey("op"))
                return BadRequest();

            if (HttpContext.Request.Headers["op"] != "Mt625")
                return BadRequest();

            var contacts = await _repo.GetAll();
            await _mailQueue.Send(contacts);
            return Ok();
        }

        [HttpPost("sms-notif")]
        public async Task<IActionResult> SendSmsReminders()
        {
            if (!HttpContext.Request.Headers.ContainsKey("op"))
                return BadRequest();

            if (HttpContext.Request.Headers["op"] != "Mt625")
                return BadRequest();

            var contacts = (await _repo.GetAll())
                .Where(c => !string.IsNullOrWhiteSpace(c.Phone));

            foreach (var c in contacts)
                await _smsQueue.Send(c, _smsReminderText);

            return Ok();

        }
    }

}