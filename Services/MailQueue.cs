using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using System.IO;
using Microsoft.Extensions.Configuration;
using CsvHelper;
using System.Text;
using System.Linq;
using NATS.Client;
using Google.Protobuf;

namespace usm
{
    public interface MailQueue
    {
        Task Send(Contact contact);
        Task Send(List<Contact> contacts);
    }

    public class MailQueueImpl : MailQueue
    {
        private readonly RenderView _renderer;
        private List<string> _sendTo;
        private string _clusterAddr;

        public MailQueueImpl(RenderView renderer, IConfiguration config)
        {
            _renderer = renderer;
            _sendTo = config.GetValue<string>("email_distribution_list", "doug.l.mitchell@gmail.com").Split(",").ToList();
            _clusterAddr = $@"nats://{config.GetValue<string>("OCS_NATS_CLUSTER_PORT_4222_TCP_ADDR")}:{config.GetValue<int>("OCS_NATS_CLUSTER_PORT_4222_TCP_PORT")}";
        }

        public async Task Send(Contact contact)
        {
            var res = await _renderer.RenderToStringAsync("Email/InterestNotification", contact);
            var mail = new ocs.MailMessage
            {
                HtmlBody = res,
                From = new ocs.Address { Name = "Online Christian Services", Email = "info@onlinechristianservices.org" },
                Subject = "Event Registration"
            };
            foreach (var recip in _sendTo)
                mail.To.Add(new ocs.Address { Email = recip });
            postMailMessage(mail);
        }

        public async Task Send(List<Contact> contacts)
        {
            // convert to csv
            var sw = new StringWriter();
            var writer = new CsvWriter(sw);
            writer.WriteRecords(contacts);

            var res = await _renderer.RenderToStringAsync("Email/AllRegistrations", null);
            var mail = new ocs.MailMessage
            {
                HtmlBody = res,
                From = new ocs.Address { Name = "Online Christian Services", Email = "info@onlinechristianservices.org" },
                Subject = "Event Registrations",
            };

            foreach (var recip in _sendTo)
                mail.To.Add(new ocs.Address { Email = recip });

            mail.Attachments.Add(new ocs.Attachment
            {
                Type = "text/csv",
                Filename = $"{DateTime.UtcNow.ToString("yyyy_MM_dd")}_interests_complete.csv",
                Data = ByteString.CopyFromUtf8(sw.ToString())
            });

            postMailMessage(mail);
        }

        private void postMailMessage(ocs.MailMessage mm)
        {
            var opts = ConnectionFactory.GetDefaultOptions();
            opts.Servers = new string[] { _clusterAddr };
            using (IConnection c = new ConnectionFactory().CreateConnection(opts))
            {
                c.Publish("ocs.email", mm.ToByteArray());
                c.Flush();
            }
        }
    }
}
