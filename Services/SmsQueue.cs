using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;

namespace usm
{
    public interface SmsQueue
    {
        Task Send(Contact contact, string message);
    }

    public class SmsQueueImpl : SmsQueue
    {
        private CloudQueue _queue;
        private async Task<CloudQueue> getQueue()
        {
            if (_queue == null)
                _queue = await QueueFactory();

            return _queue;
        }

        private async Task<CloudQueue> QueueFactory()
        {
            var acct = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=ocsstore;AccountKey=8wrNq1ozGulfM0R6dC97pFyVLM/8mGFLPlT7E5f4h1cCcnzHCBclQ8Vct9+teDR4M/U008kLvDWny9a90EfR7Q==;EndpointSuffix=core.windows.net");
            var client = acct.CreateCloudQueueClient();
            var queue = client.GetQueueReference("sms-notifications-se");
            await queue.CreateIfNotExistsAsync();
            return queue;
        }
        public async Task Send(Contact contact, string message)
        {
            var msg = new
            {
                to = contact.Phone,
                message = message
            };

            var json = JsonConvert.SerializeObject(msg);
            var q = await getQueue();
            await q.AddMessageAsync(new CloudQueueMessage(json));
        }
    }
}